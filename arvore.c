/*--------------------------------------------------------------------------------------------------------------------------------------
Apresenta o resultado dos percursos em pré, em e pós-órdem para diferentes árvores.
Ex: 
(a) 4.31.2...(enter)
(b) 72..483..15..6...(enter)
(c) 628.47..3..1.5...(enter)

(a) 4		(b) 7			(c) 6			
	 \		   / \			   / \
	  3		  2   4			  2   1	  
	 /			 /			 /	   \
	1			8			8       5
	 \		   / \			 \
	  2		  3	  1			  4	 
	  			 / \		 / \	
	  			5 	6		7	3
---------------------------------------------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

typedef struct no *ptno;
struct no{
	char info;
	ptno esq, dir;
};

ptno Arvore;

void pre_ordem(ptno A){
	if(A){
		printf("%c ", A->info);
		pre_ordem(A->esq);
		pre_ordem(A->dir);
	}
}

void em_ordem(ptno A){
	if(A){
		em_ordem(A->esq);
		printf("%c ", A->info);
		em_ordem(A->dir);
	}
}

void pos_ordem(ptno A){
	if(A){
		pos_ordem(A->esq);
		pos_ordem(A->dir);
		printf("%c ",A->info);
	}
}

ptno constroi (){
	char c = getchar();
	if(c == '.')
		return NULL;
	else{
		ptno A = (ptno) malloc (sizeof(struct no));
		A->info = c;
		A->esq = constroi();
		A->dir = constroi();
		return A;
	}
}

void main(){
	ptno T = NULL;
	T = constroi();
	printf("Pré: ");
	pre_ordem(T);
	printf("\nEm: ");
	em_ordem(T);
	printf("\nPós: ");
	pos_ordem(T);
	printf("\n");
}